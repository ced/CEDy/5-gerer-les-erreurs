# Définition variables globales
dico_lib = {
    'distance': """\
|| Quelle est la distance entre le domicile et le lieu de travail
|| (0 pour quitter l'application) ? """,
    'deplacement': """\
|| Choisir le {} mode de déplacement :
|| 1 - train
|| 2 - vélo
|| 3 - voiture
|| 4 - quitter
|| Mon choix: """,
    1: "premier",
    2: "second",
    'train': "le train",
    'velo': "le vélo",
    'voiture': "la voiture",
    'choix_1': 'Je choisis {}',
    'choix_2': 'plutôt que {}',
    'IOError': "|| Impossible de lire le fichier {}.txt!",
    'ValueError': "|| Erreur lors de la conversion en entier !",
}
dico_cout = {
    'train': (14.62, 9.26),
    'velo': (0, 0),
    'voiture': (129.62, 50.65),
    'lab_titres': ("Effet de serre", "Énergie"),
    'lab_unites': ("{} kg eq. CO2", "{} l eq. pétrole"),
    'lab_eco': ("J'évite {} kg eq. CO2 par an",
                "Je consomme {} litres eq. pétrole en moins par an"),
    'lab_dep': ("J'émets {} kg eq. CO2 en plus par an",
                "Je consomme {} litres eq. pétrole en plus par an"),
}
