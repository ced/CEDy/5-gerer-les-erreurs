"""
    CEDy.calculette
    ~~~~~~~~~~~~~~~

    Le module implémente la définition des fonctions
    d'une calculette eco-déplacements, permettant de
    calculer l'impact de nos déplacements quotidiens
    sur l'environnement et nos dépenses.

    :copyright: (c) 2014 par moulinux.
    :license: GPLv3, voir la LICENCE pour plus d'informations.
"""
import sys
import os


def charge_en_tete(nom):
    """
    Charge l'en-tête depuis le fichier nom.txt

    nom : nom du fichier contenant l'en-tête (sans l'extension .txt)

    Valeur de retour : Tuple contenant les données d'en-tête
    """
    fic = open(nom + ".txt", "r", encoding="utf-8")
    data = fic.readlines()
    fic.close()

    for i in range(len(data)):
        data[i] = data[i].strip()

    return tuple(data)


def affiche_en_tete(tup):
    """
    Affichage des différents d'en-tête de l'application.

    tup : tuples contenant les éléments d'en-tête

    pas de valeur de retour
    """
    for i in range(len(tup) - 1):
        print(tup[i])


def ajouter_log(nom, km, dep1, dep2):
    """
    Écriture dans un fichier de log.

    nom : nom du fichier de log (sans l'extension .log)
    km : distance entre le domicile et le lieu de travail
    dep1 : premier mode de déplacement saisie
    dep2 : second mode de déplacement saisie

    pas de valeur de retour
    """
    import time
    str_date = time.strftime('%Y-%m-%d-%H:%M:%S', time.localtime())
    fic = open(nom + ".log", "a")
    fic.write("{}; km={}; dep1={}; dep2={}\n".format(str_date, km, dep1, dep2))
    fic.close()


def efface_ecran():
    """
    Efface l'écran de la console.
    """
    if sys.platform.startswith("win"):
        # Si système Windows
        os.system("cls")
    else:
        # Si système GNU/Linux ou OS X
        os.system("clear")


def affiche_cout(tab, nb_tab):
    """
    Affichage des différents éléments d'un tableau, séparés par une tabulation.

    tab : liste des éléments
    nb_tab : entier représentant le nombre de tabulations à afficher

    pas de valeur de retour
    """
    if nb_tab != 0:
        for e in tab:
            print(e, end="\t"*(nb_tab - int(len(e)/15)))
        print()


def affiche_cout_km(les_couts, les_unites_couts, nb_tab):
    """
    Affichage des coûts de déplacement.

    les_couts : liste des coûts kilométriques pour un mode de déplacement
    les_unites_couts : liste des unités associées aux coûts kilométriques
    nb_tab : entier représentant le nombre de tabulations à afficher

    pas de valeur de retour
    """
    indice = 0
    for un_cout in les_couts:
        str_cout = les_unites_couts[indice].format(round(abs(un_cout), 1))
        if nb_tab == 0:
            fin = "\n"
            print("|| ", str_cout, end=fin)
        else:
            fin = "\t"*(nb_tab - int(len(str_cout)/15))
            print(str_cout, end=fin)
        indice = indice + 1
    if nb_tab != 0:
        print()


def calcule_cout_km(les_couts, km):
    """
    Calcule les coûts de déplacement.

    les_couts : liste des coûts pour 1 km d'un mode de déplacement
    km : distance entre le domicile et le lieu de travail

    retourne une liste des coûts kilométriques liés à un mode de déplacement
    """
    res = list(range(len(les_couts)))   # Initialiser la liste des résultats
    indice = 0
    for un_cout in les_couts:
        res[indice] = round(un_cout * km, 1)
        indice = indice + 1
    return res


def compare_cout_km(les_couts_1, les_couts_2):
    """
    Compare les coûts de déplacement.

    les_couts_1 : liste des coûts calculés pour le premier mode de déplacement
    les_couts_2 : liste des coûts calculés pour le second mode de déplacement

    retourne une liste contenant les coûts kilométriques comparés
    """
    diff = list(range(len(les_couts_1)))  # Initialiser la liste des résultats
    for indice in range(len(les_couts_1)):
        diff[indice] = les_couts_2[indice] - les_couts_1[indice]
    return diff


def choix_entier(message):
    """
    Demande à l'utilisateur de saisir un entier.

    message : question posée à l'utilisateur

    retourne un entier
    """
    return int(input(message))


def choix_deplacement(dico, km, cout_km, dep, choix_utilisateur):
    """
    Demande à l'utilisateur de saisir son mode de déplacement.

    dico : dictionnaire contenant la définition des en-têtes et des menus
    km : distance entre le domicile et le lieu de travail (entier)
    cout_km : dictionnaire contenant des listes de coûts pour les déplacements
    dep : entier représentant l'itération sur le mode de déplacement
    choix_utilisateur : dictionnaire mémorisant les choix des l'utilisateur

    retourne une liste des coûts kilométriques liés à un mode de déplacement
    """
    val_dep = choix_entier(dico['deplacement'].format(dico[dep]))
    if val_dep == 1:
        choix_utilisateur[dep] = dico['train']
        return calcule_cout_km(cout_km['train'], km)
    elif val_dep == 2:
        choix_utilisateur[dep] = dico['velo']
        return calcule_cout_km(cout_km['velo'], km)
    elif val_dep == 3:
        choix_utilisateur[dep] = dico['voiture']
        return calcule_cout_km(cout_km['voiture'], km)
    elif val_dep == 4:
        exit(0)


def run(dico, cout_km, nb_tab):
    """
    Boucle principale de l'application. Affiche l'en-tête et les menus.

    dico : dictionnaire contenant la définition des en-têtes et des menus
    cout_km : dictionnaire contenant des listes de coûts pour les déplacements
    nb_tab : entier représentant le nombre de tabulations à afficher

    pas de valeur de retour
    """
    efface_ecran()
    affiche_en_tete(dico['app'])
    while True:
        choix = {}          # initialiser les choix de l'utilisateur
        km = choix_entier(dico['distance'])
        if km == 0:
            break
        print(dico['app'][len(dico['app'])-1])
        # initialiser la liste des coûts pour le premier mode de déplacement
        mes_couts_dep_1 = choix_deplacement(dico, km, cout_km, 1, choix)
        print(dico['app'][len(dico['app'])-1])
        # initialiser la liste des coûts pour le second mode de déplacement
        mes_couts_dep_2 = choix_deplacement(dico, km, cout_km, 2, choix)
        print(dico['app'][len(dico['app'])-1])
        # comparer les coûts des deux modes de déplacement
        diff_couts = compare_cout_km(mes_couts_dep_1, mes_couts_dep_2)
        taille = nb_tab + 1
        efface_ecran()
        affiche_en_tete(dico['app'])
        print("|| ", end="\t"*taille)
        affiche_cout(cout_km['lab_titres'], nb_tab)
        print("|| ", dico['choix_1'].format(choix[1]))
        print("|| ", end="\t"*taille)
        affiche_cout_km(mes_couts_dep_1, cout_km['lab_unites'], nb_tab)
        print("|| ", dico['choix_2'].format(choix[2]))
        print("|| ", end="\t"*taille)
        affiche_cout_km(mes_couts_dep_2, cout_km['lab_unites'], nb_tab)
        if diff_couts[0] < 0:
            affiche_cout_km(diff_couts, cout_km['lab_dep'], 0)
        else:
            affiche_cout_km(diff_couts, cout_km['lab_eco'], 0)
        print(dico['app'][len(dico['app'])-1])
