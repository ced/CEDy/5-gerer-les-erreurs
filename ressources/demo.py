erreur = 0

try:
    fic = open("mon_fichier.txt", "r")
    print("conversion en entier :", int("une chaîne"))
except IOError:
    print("Impossible d'ouvrir le fichier !")
    exit(1)
except ValueError:
    print("Erreur lors de la conversion en entier !")
    erreur = erreur + 1

print("On continue l'exécution...")
