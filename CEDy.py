#!/usr/bin/python3
import calculette as cal
import dict_fr as glob


if __name__ == "__main__":
    nb_tab = 2
    # chargement du logo
    en_tete = cal.charge_en_tete("logo_fr")
    glob.dico_lib['app'] = en_tete
    cal.run(glob.dico_lib, glob.dico_cout, nb_tab)
