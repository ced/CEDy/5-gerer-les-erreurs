**Rappel** : Bien que la calculette de l'Ademe présente une interface graphique (GUI : Graphic User Interface), nous allons dans un premier temps, nous concentrer sur un programme en ligne de commandes (CLI : Command Line Interface).
**Écrire un programme CLI**, par rapport à un un programme GUI, présente de nombreux avantages, ne serait-ce que l'économie de moyens nécessaires au fonctionnement.
Un programme CLI sera surtout **beaucoup plus facile à écrire** dans un premier temps car nous n'aurons pas à interagir avec le serveur graphique et le gestionnaire de fenêtres et autres composants graphiques.

## Notre mission

Lorsque l'on développe un programme, il y a une partie essentielle à ne pas négliger :
la **gestion des erreurs**. On ne peut pas laisser l'utilisateur complètement
perdu face à un écran affichant un message incompréhensible...
